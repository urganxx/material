# -*- coding: utf-8 -*-
__author__ = 'urganxx'
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate
from django import forms
from django.forms.widgets import RadioSelect, TextInput, PasswordInput, HiddenInput, Textarea, FileInput
from django.forms.extras.widgets import SelectDateWidget
import datetime
from datetime import date

STATUS = (
    (0, u'小姐'),
    (1, u'先生'),
)

Categories = (
    (0, 'Development'),
    (1, 'OurStory'),
    (2, 'Industries'),
    (3, 'Others'),
)

class HorizRadioRenderer(forms.RadioSelect.renderer):
    """ this overrides widget method to put radio buttons horizontally instead of vertically."""
    def render(self):
        """Outputs radios"""
        return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))


class AuthenticationForm(forms.Form):
    username = forms.CharField(label='Username', widget=TextInput(attrs={'placeholder':'Username','name':'username','id':'input-username','class':'required'}))
    password = forms.CharField(label='Password', widget=TextInput(attrs={'placeholder':'Password','name':'password','id':'input-password','class':'required', 'type':'password'}))

    def __init__(self, request=None, *args, **kwargs):
        """
        If request is passed in, the form will validate that cookies are
        enabled. Note that the request (a HttpRequest object) must have set a
        cookie with the key TEST_COOKIE_NAME and value TEST_COOKIE_VALUE before
        running this validation.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError("Please enter a correct username and password. Note that both fields are case-sensitive.")
            elif not self.user_cache.is_active:
                raise forms.ValidationError("This account is inactive.")

        #TODO: determine whether this should move to its own method.
        if self.request:
            if not self.request.session.test_cookie_worked():
                raise forms.ValidationError("Your Web browser doesn't appear to have cookies enabled. Cookies are required for logging in.")

        return self.cleaned_data

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class AddCustomerForm(forms.Form):
    #id = forms.CharField(widget=HiddenInput(), required=True)
    company = forms.CharField(label=u'Company Name: ', widget=TextInput(attrs={'class':'form-control','placeholder':'Company Name:'}),required=False)
    email = forms.EmailField(label=u'Email: ',widget=TextInput(attrs={'class':'form-control','name':'email','placeholder':'Email:'}), required=True)
    bill_address = forms.CharField(label=u'Bill Address: ',widget=TextInput(attrs={'class':'form-control','name':'bill-addr','placeholder':'Bill Address:'}),required=False)
    phoneNum = forms.IntegerField(label=u'Phone Number: ', widget=TextInput(attrs={'class':'form-control','id':'phone','name':'digits1','placeholder':'Phone Number:'}), required=True)

class SaveCustomerForm(forms.Form):
    #id = forms.CharField(widget=HiddenInput(), required=True)
    company = forms.CharField(label=u'Company Name: ', widget=TextInput(attrs={'class':'form-control','placeholder':'Company Name:'}),required=False)
    email = forms.EmailField(label=u'Email: ',widget=TextInput(attrs={'class':'form-control','name':'email','placeholder':'Email:'}),required=True)
    bill_address = forms.CharField(label=u'Bill Address: ',widget=TextInput(attrs={'class':'form-control','name':'bill-addr','placeholder':'Bill Address:'}),required=False)
    phoneNum = forms.CharField(label=u'Phone Number: ', widget=TextInput(attrs={'class':'form-control','id':'phone','name':'digits1','placeholder':'Phone Number:'}), required=True)

class SpecForm(forms.Form):
    #id = forms.CharField(widget=HiddenInput(), required=True)
    name = forms.CharField(label=u'中文名字', widget=TextInput(attrs={'placeholder': 'Item-Name', 'class': 'form-control' }), required=False)
    eng_name = forms.CharField(label=u'英文名字', widget=TextInput(attrs={'class': 'form-control','placeholder': 'Item-Name Eng' }), required=False)
    interface = forms.CharField(label=u'使用介面', widget=TextInput(attrs={'class': 'form-control','placeholder': u'使用介面'}), required=False)
    coverage = forms.CharField(label=u'覆蓋角度', widget=TextInput(attrs={'class': 'form-control','placeholder': u'覆蓋角度'}), required=False)
    distance = forms.CharField(label=u'使用距離', widget=TextInput(attrs={'class': 'form-control','placeholder': u'使用距離'}), required=False)
    speeds = forms.CharField(label=u'傳輸速度', widget=TextInput(attrs={'class': 'form-control','placeholder': u'傳輸速度'}), required=False)
    support = forms.CharField(label=u'支援版本', widget=TextInput(attrs={'class': 'form-control','placeholder': u'支援版本'}), required=False)
    platforms = forms.CharField(label=u'支援平台', widget=TextInput(attrs={'class': 'form-control','placeholder': u'支援平台'}), required=False)
    flowchart = forms.FileField(label=u'流程圖', widget=FileInput(attrs={'class': 'form-control','placeholder': u'流程圖'}), required=False)
    prices = forms.CharField(label=u'公定價', widget=TextInput(attrs={'class': 'form-control','placeholder': u'公定價'}), required=False)
    launch_date = forms.DateField(label=u'上市時間', widget=SelectDateWidget(attrs={'class':'col-md-4'}), required=False)
    comments = forms.CharField(label=u'備註', widget=Textarea(attrs={'class': 'form-control','placeholder': u'備註'}), required=False)

class NewsFeedForm(forms.Form):
    #id = forms.CharField(widget=HiddenInput(), required=True)
    title = forms.CharField(label=u'標題', widget=TextInput(attrs={'class': 'form-control'}), required=False)
    category = forms.ChoiceField(label=u'文章分類', choices=Categories, widget=forms.Select(attrs={'class':'col-md-12'}), required=False)
    public_date = forms.DateField(label=u'發表日期', widget=SelectDateWidget(attrs={'class':'col-md-4'}), required=True)
    content = forms.CharField(label=u'文章內容', widget=Textarea(attrs={'class': 'form-control','placeholder': u'文章內容'}), required=False)

class VideoForm(forms.Form):
    #id = forms.CharField(widget=HiddenInput(), required=True)
    title = forms.CharField(label=u'影片標題', widget=TextInput(attrs={'placeholder': u'影片標題', 'class':'form-control'}), max_length=50, required=True)
    content = forms.CharField(label=u'影片描述', widget=Textarea(attrs={'placeholder': u'影片描述', 'class':'form-control'}), required=False)
    video_url = forms.URLField(label=u'影片網址', widget=TextInput(attrs={'placeholder': u'影片網址', 'class':'form-control'}), required=True)
    #update_date = forms.DateField(auto_created=True, auto_now_add=True, verbose_name=u'影片建立時間')

class PartnershipForm(forms.Form):
    #id = forms.CharField(widget=HiddenInput(), required=True)
    company = forms.CharField(label=u'公司名稱', widget=TextInput(attrs={'placeholder': u'公司名稱','class':'form-control'}), required=True)
    trademark= forms.FileField(label=u'公司商標', required=False)
    website = forms.URLField(label=u'公司網址', widget=TextInput(attrs={'placeholder': u'公司網址', 'class':'form-control'}), required=True)
    slogan = forms.CharField(label=u'關鍵一句話', widget=TextInput(attrs={'placeholder': u'關鍵一句話', 'class':'form-control'}), required=False)
    testimonial = forms.CharField(label=u'推薦述', widget=Textarea(attrs={'placeholder':u'推薦述', 'class':'form-control'}))