# -*- coding: utf-8 -*-
# Create your views here.
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.contrib.messages import info, error
from forms import NewsFeedForm, SpecForm, AddCustomerForm, SaveCustomerForm, VideoForm, PartnershipForm, AuthenticationForm
from about.models import NewsFeed, Video, Partnership
from products.models import Spec
from resources.models import Customers

import urllib, urlparse, Cookie
import datetime, json

@login_required(login_url='/login/')
def index(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['Slogan'] = u'管理頁面'
    context['form1'] = SpecForm()
    context['form2'] = NewsFeedForm()
    context['form3'] = VideoForm()
    context['form4'] = PartnershipForm()
    context['form5'] = AddCustomerForm()
    return render_to_response('adminPanel/index.html', context)

def logout(request):
    auth_logout(request)
    info(request, _("Successfully logged out"))
    return redirect("/")

@login_required(login_url='/login/')
def createSpec(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['form1'] = SpecForm
    return render_to_response('adminPanel/index.html', context)

@login_required(login_url='/login/')
def editSpec(request, spec_id):
    context = RequestContext(request)
    context.update(csrf(request))
    raw_data = Spec.objects.get(id=spec_id)
    resp = dict()
    resp['name'] = raw_data.name
    resp['eng_name'] = raw_data.eng_name
    resp['coverage'] = raw_data.coverage
    resp['distance'] = raw_data.distance
    resp['comments'] = raw_data.comments
    #resp['flowchart'] = raw_data.flowchart
    resp['interface'] = raw_data.interface
    #resp['launch_date'] = raw_data.launch_date
    resp['platforms'] = raw_data.platforms
    resp['prices'] = raw_data.prices
    resp['speeds'] = raw_data.speeds
    resp['support'] = raw_data.support
    msg = json.dumps(resp)
    return HttpResponse(msg, mimetype='application/json')

@login_required(login_url='/login/')
def saveSpec(request):
    context = RequestContext(request)
    context.update(csrf(request))
    if request.method == "POST":
        form = SpecForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form_data = form.cleaned_data
            product = Spec()
            product.name = form_data['name']
            product.eng_name = form_data['eng_name']
            product.interface = form_data['interface']
            product.coverage = form_data['coverage']
            product.distance = form_data['distance']
            product.speeds = form_data['speeds']
            product.support = form_data['support']
            product.platforms = form_data['platforms']
            product.flowchart = request.FILES['flowchart']
            product.prices = form_data['prices']
            product.launch_date = form_data['launch_date']
            product.comments = form_data['comments']
            product.save()
            info(request, _("Successfully Saved Your Form!"))
        else:
            print form
            error(request, _("Some Fields Have lost data!"))
    else:
        error(request, _("There are some errors when sending form!"))

    return redirect('/Admin/')

@login_required(login_url='/login/')
def createNewsFeed(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['form'] = NewsFeedForm()
    return render_to_response('adminPanel/index.html', context)

@login_required(login_url='/login/')
def editNewsFeed(request, news_id):
    context = RequestContext(request)
    context.update(csrf(request))
    raw_data = NewsFeed.objects.get(id=news_id)
    resp = dict()
    resp['title'] = raw_data.title
    resp['category'] = raw_data.category
    resp['content'] = raw_data.content
    resp['public_date'] = raw_data.public_date
    msg = json.dumps(resp)
    return HttpResponse(msg, mimetype='application/json')

@login_required(login_url='/login/')
def saveNewsFeed(request):
    context = RequestContext(request)
    context.update(csrf(request))
    if request.method == "POST":
        form = NewsFeedForm(data=request.POST)
        if form.is_valid():
            raw_data = form.cleaned_data
            result = NewsFeed()
            result.title = raw_data['title']
            result.category = raw_data['category']
            result.content = raw_data['content']
            result.save()
            info(request, _("Successfully Saved Your Form!"))
        else:
            error(request, _("There are some errors when validating form!"))
    else:
        error(request, _("There are some errors when sending form!"))

    return redirect('/Admin/')

@login_required(login_url='/login/')
def createVideo(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['form'] = VideoForm()
    return render_to_response('adminPanel/index.html', context)

@login_required(login_url='/login/')
def editVideo(request, video_id):
    context = RequestContext(request)
    context.update(csrf(request))
    raw_data = Video.objects.get(id=video_id)
    resp = dict()
    resp['title'] = raw_data.title
    resp['video_url'] = raw_data.video_url
    resp['content'] = raw_data.content
    msg = json.dumps(resp)
    return HttpResponse(msg, mimetype='application/json')

def saveVideo(request):
    context = RequestContext(request)
    context.update(csrf(request))
    if request.method == "POST":
        form = VideoForm(data=request.POST)
        if form.is_valid():
            raw_data = form.cleaned_data
            result = Video(title=raw_data['title'], video_url=raw_data['video_url'], content=raw_data['content'])
            result.save()
            info(request, _("Successfully Saved Your Form!"))
        else:
            print form
            error(request, _("There are some errors when validating form!"))

    else:
        error(request, _("There are some errors when sending form!"))

    return redirect('/Admin/')

@login_required(login_url='/login/')
def createPartner(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['form'] = PartnershipForm()
    return render_to_response('adminPanel/index.html', context)

@login_required(login_url='/login/')
def editPartner(request, partner_id):
    context = RequestContext(request)
    context.update(csrf(request))
    raw_data = Partnership.objects.get(id=partner_id)
    resp = dict()
    resp['company'] = raw_data.company
    resp['slogan'] = raw_data.slogan
    resp['testimonial'] = raw_data.testimonial
    #resp['trademark'] = raw_data.trademark
    resp['website'] = raw_data.website
    msg = json.dumps(resp)
    return HttpResponse(msg, mimetype='application/json')

@login_required(login_url='/login/')
def savePartner(request):
    context = RequestContext(request)
    context.update(csrf(request))
    if request.method == "POST":
        form = PartnershipForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form_data = form.cleaned_data
            raw = Partnership()
            raw.company = form_data['company']
            raw.slogan = form_data['slogan']
            raw.testimonial = form_data['testimonial']
            raw.trademark = request.FILES['trademark']
            raw.website = form_data['website']
            raw.save()
            info(request, _("Successfully Saved Your Form!"))
        else:
            error(request, _("There are some errors when validating form!"))
    else:
        error(request, _("There are some errors when sending form!"))

    return redirect('/Admin/')

@login_required(login_url='/login/')
def createCustomer(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['form'] = AddCustomerForm()
    return render_to_response('adminPanel/index.html', context)

@login_required(login_url='/login/')
def editCustomer(request, customer_id):
    context = RequestContext(request)
    context.update(csrf(request))
    raw_data = Customers.objects.get(id=customer_id)
    resp = dict()
    resp['company'] = raw_data.company
    resp['email'] = raw_data.email
    resp['bill_address'] = raw_data.bill_address
    msg = json.dumps(resp)
    return HttpResponse(msg, mimetype='application/json')

@login_required(login_url='/login/')
def saveCustomer(request):
    context = RequestContext(request)
    context.update(csrf(request))
    if request.method == "POST":
        form = SaveCustomerForm(data=request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            raw_data = Customers(company=form_data['company'], email=form_data['email'], bill_address=form_data['bill_address'])
            raw_data.save()
            info(request, _("Successfully Saved Your Form!"))
        else:
            error(request, _("Some Fields Have lost data!"))
    else:
        error(request, _("There are some errors when sending form!"))

    return redirect('/Admin/')
