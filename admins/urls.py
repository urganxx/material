__author__ = 'urganxx'
from settings import PROJECT_DIRNAME

urlpatterns = patterns('admins.views',
    url( r'^products/add/', 'createSpec' ),
    url( r'^products/edit/(?P<spec_id>\d+)/', 'editSpec' ),
    url( r'^products/save/', 'saveSpec' ),

    url( r'^news/add/', 'createNewsFeed' ),
    url( r'^news/edit/(?P<news_id>\d+)/', 'editNewsFeed' ),
    url( r'^news/save/', 'saveNewsFeed' ),

    url( r'^video/add/', 'createVideo' ),
    url( r'^video/edit/(?P<video_id>\d+)/', 'editVideo' ),
    url( r'^video/save/', 'saveVideo' ),

    url( r'^partners/add/', 'createPartner' ),
    url( r'^partners/edit/(?P<partner_id>\d+)/', 'editPartner' ),
    url( r'^partners/save/', 'savePartner' ),

    url( r'^customers/add/', 'createCustomer' ),
    url( r'^customers/edit/(?P<customer_id>\d+)/', 'editCustomer' ),
    url( r'^customers/save/', 'saveCustomer' ),

    url( r'^$', 'index' ),
)
