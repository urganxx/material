# -*- coding: utf-8 -*-
from django.db import models
import datetime

class NewsFeed(models.Model):
    title = models.CharField(verbose_name=u'標題', max_length=120)
    featurePic = models.ImageField(max_length=250, verbose_name=u'新聞圖', upload_to='static/images/news/')
    category = models.IntegerField(max_length=4, verbose_name=u'分類')
    public_date = models.DateField(auto_now_add=True, verbose_name=u'發表日期')
    content = models.TextField(verbose_name=u'文章內容')
    external_link=models.URLField(max_length=255, verbose_name=u'外部連結')

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.title, self.category, self.public_date, self.content)

    class Meta:
        verbose_name_plural = "News"


class Video(models.Model):
    title = models.CharField(max_length=50,verbose_name=u'影片標題')
    content = models.TextField(verbose_name=u'影片描述')
    update_date = models.DateField(auto_created=True, auto_now_add=True, verbose_name=u'影片建立時間')
    video_url = models.URLField(verbose_name=u'影片網址')

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.title, self.content, self.update_date, self.video_url)

    class Meta:
        verbose_name_plural = "Videos"


class Partnership(models.Model):
    company = models.CharField(max_length=50, verbose_name=u'公司名稱')
    trademark= models.ImageField(max_length=250, verbose_name=u'公司商標', upload_to='static/images/partnerships/')
    website = models.URLField(verbose_name=u'公司網址')
    slogan = models.CharField(max_length=50, verbose_name=u'關鍵一句話')
    testimonial = models.TextField(verbose_name=u'推薦書')

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.company, self.website, self.slogan, self.testimonial)

    class Meta:
        verbose_name_plural = "Partnerships"


class MainEvents(models.Model):
    Events = models.CharField(max_length=50,verbose_name=u'主要大事')
    snapshot = models.ImageField(max_length=250, verbose_name=u'示意圖', upload_to='static/images/history/')
    Date = models.DateField(auto_created=False, auto_now_add=False, verbose_name=u'事件發生時間')
    Description = models.TextField(verbose_name=u'文字描述')

    def __unicode__(self):
        return u'%s, %s, %s' % (self.Events, self.Date, self.Description)


class Facilities(models.Model):
    FacilityName = models.CharField(max_length=50,verbose_name=u'設備廠商')
    FacilityPic = models.ImageField(max_length=250, verbose_name=u'廠商圖', upload_to='static/images/Facility/')
    FacilityIntro= models.TextField(verbose_name=u'廠商介紹')

    def __unicode__(self):
        return u'%s, %s' % (self.FacilityName, self.FacilityIntro)


class Exhibitions(models.Model):
    Name = models.CharField(max_length=250,verbose_name=u'活動名稱')
    Snapshot = models.URLField(verbose_name=u'活動宣傳圖')
    BeginDate= models.DateField(auto_created=False, auto_now_add=False, verbose_name=u'展期開始時間')
    FinalDate= models.DateField(auto_created=False, auto_now_add=False, verbose_name=u'展期結束時間')
    Booth = models.CharField(max_length=50,verbose_name=u'攤位代號')
    Country = models.CharField(max_length=50,verbose_name=u'佈展國家')
    ExhUrls = models.URLField(verbose_name=u'活動網址')

    def __unicode__(self):
        return u'%s, %s, %s, %s, %s, %s' % (self.Name, self.BeginDate, self.FinalDate, self.Booth, self.Country, self.ExhUrls)


class LatestLog(models.Model):
    describe = models.CharField(max_length=250,verbose_name=u'更新紀錄敘述')
    pubDate= models.DateField(auto_created=False, auto_now_add=False, verbose_name=u'更新時間')

    def __unicode__(self):
        return u'%s, %s' % (self.describe, self.pubDate)


class Information(models.Model):
    company = models.CharField(max_length=250, verbose_name=u'公司名稱')
    address = models.CharField(max_length=250, verbose_name=u'公司地址')
    address_map = models.URLField(verbose_name=u'公司位置 In Google Map')
    telephone = models.CharField(max_length=20, verbose_name=u'公司電話')
    support_mail = models.EmailField(verbose_name=u'公司支援信箱', unique=True)
    service_mail = models.EmailField(verbose_name=u'公司服務信箱', unique=True)
    twitter = models.URLField(verbose_name=u'公司twitter')
    fb_fans = models.URLField(verbose_name=u'公司Facebook Fans')
    youtube = models.URLField(verbose_name=u'公司Youtube Channel')

    def __unicode__(self):
        return u'%s, %s, %s, %s, %s, %s, %s, %s, %s' % (self.company, self.address, self.address_map, self.telephone,
                                                        self.support_mail, self.service_mail)
