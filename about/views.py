# -*- coding: utf-8 -*-
# Create your views here.
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render, redirect, render_to_response
from django.core.context_processors import csrf
from models import NewsFeed, MainEvents, Facilities
from django.core.paginator import Paginator


def index(request):
    context = RequestContext(request)
    context.update(csrf(request))
    result = NewsFeed.objects.all().order_by('-public_date')
    newsList = Paginator(result, 4)
    page_num = 1
    if request.GET.get('page'):
        page_num = request.GET.get('page')

    page_list = newsList.page(page_num)
    context['current'] = page_num
    context['total_page'] = newsList.num_pages
    context['pagination'] = newsList.page_range
    if context['total_page'] > page_num:
        context['next'] = page_num + 1

    context['output'] = page_list.object_list
    return render_to_response('about/index.html', context)


def detail_view(request, news_id):
    context = RequestContext(request)
    try:
        context['news'] = NewsFeed.objects.get(id=news_id)
    except NewsFeed.DoesNotExist:
        context['news'] = NewsFeed.objects.get_empty_query_set()

    context['list'] = NewsFeed.objects.all().exclude(id=news_id).order_by('-public_date')[:4]
    return render_to_response('about/blog-single.html', context)


def detail(request, fac_id):
    context = RequestContext(request)
    try:
        context['news'] = Facilities.objects.get(id=fac_id)
    except Facilities.DoesNotExist:
        context['news'] = Facilities.objects.get_empty_query_set()

    context['list'] = Facilities.objects.all().exclude(id=fac_id).order_by('id')[:2]
    return render_to_response('about/blog-single.html', context)


def facilities(request):
    context = RequestContext(request)
    result = Facilities.objects.all().order_by('id')
    faciList = Paginator(result, 6)
    page_num = 1
    if request.GET.get('page'):
        page_num = request.GET.get('page')

    page_list = faciList.page(page_num)
    context['current'] = page_num
    context['total_page'] = faciList.num_pages
    context['pagination'] = faciList.page_range
    if context['total_page'] > page_num:
        context['next'] = page_num + 1

    context['output'] = page_list.object_list
    return render_to_response('about/facilities.html', context)


def history(request):
    context = RequestContext(request)
    main = "Millitronic is a millimeter wave solution company specializing in the development of 60 GHz/WiGig application system for 5G ecosystem. Millitronic’s goal is to provide WiGig solutions for the commercial electronics low latency video streaming and gigabit networking market, and specifically for mixed reality, wireless AI edge computing, and wireless medical equipments."
    sec = "Millitronic is a venture capital funded by NCTU angle club, and innodisk from October 2011. The core technology and IP patents consist of millimeter radio integration, beamforming phase array, software defined network, wireless low latency image process, and MMW AI computing. We have own fabricate production line, software, hardware, firmware team in Taiwan, which provides high quality and customized service."
    context['main'] = main
    context['second'] = sec
    context['time_line'] = MainEvents.objects.all().order_by('-Date')
    return render_to_response('about/history.html', context)