# -*- coding: utf-8 -*-
__author__ = 'urganxx'
from django.contrib import admin
from models import NewsFeed, Video, Partnership, MainEvents, Facilities, Exhibitions, LatestLog, Information


class NewsFeedAdmin(admin.ModelAdmin):
    list_display = ('title', 'featurePic', 'category', 'public_date', 'content')


class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'update_date', 'video_url')


class PartnershipAdmin(admin.ModelAdmin):
    list_display = ('company', 'trademark', 'website', 'slogan', 'testimonial')


class MainEventsAdmin(admin.ModelAdmin):
    list_display = ('Events', 'snapshot', 'Date', 'Description')


class FacilitiesAdmin(admin.ModelAdmin):
    list_display = ('FacilityName', 'FacilityPic', 'FacilityIntro')


class ExhibitionsAdmin(admin.ModelAdmin):
    list_display = ('Name', 'Snapshot', 'BeginDate', 'FinalDate', 'Booth', 'Country', 'ExhUrls')


class LatestLogAdmin(admin.ModelAdmin):
    list_display = ('describe', 'pubDate')


class InformationAdmin(admin.ModelAdmin):
    list_display = ('company', 'address', 'address_map', 'telephone', 'support_mail', 'service_mail', 'twitter', 'fb_fans', 'youtube')


admin.site.register(NewsFeed, NewsFeedAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Partnership, PartnershipAdmin)
admin.site.register(MainEvents, MainEventsAdmin)
admin.site.register(Facilities, FacilitiesAdmin)
admin.site.register(Exhibitions, ExhibitionsAdmin)
# admin.site.register(LatestLog, LatestLogAdmin)
# admin.site.register(LatestLog, InformationAdmin)
