__author__ = 'urganxx'
from django.conf.urls import patterns, include, url
#from settings import PROJECT_DIRNAME

urlpatterns = patterns('about.views',
    #url( r'list/$', 'index' ),
    url( r'facility/$', 'facilities' ),
    url( r'facility/(?P<fac_id>\d+)/$', 'detail' ),
    url( r'history/$', 'history' ),
    url( r'^$', 'index' ),
)
