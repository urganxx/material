# -*- coding: utf-8 -*-
# Create your views here.
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from accounts.models import Profile
from models import Customers, ForumRegisters, Solution, Technology
from forms import ForumRegistersForm, industries, stages
from about.models import Exhibitions
from django.core.paginator import Paginator
from django.contrib.messages import info, error

import requests

reCaptcha_secret = '6LdiOBQUAAAAALkFcLL3aG7qaiiXbLPsQEB8xb39'

def index(request):
    context = RequestContext(request)
    context.update(csrf(request))

    return render_to_response('resources/index.html', context)


#@login_required(login_url='/login/')
def services(request):
    context = RequestContext(request)
    context.update(csrf(request))

    return render_to_response('resources/services.html', context)


def applications(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['inApplications'] = True
    context['solutions'] = Solution.objects.all()
    return render_to_response('solutions/applications.html', context)


def technology(request):
    context = RequestContext(request)
    context.update(csrf(request))
    nodes = Technology.objects.all()
    pages = Paginator(nodes, 2)
    print pages.num_pages

    rows = list()
    for idx in range(1, pages.num_pages+1):
        page = pages.page(idx)
        rows.append(page.object_list)

    context['nodes'] = rows
    return render_to_response('solutions/technology.html', context)


def event_page(request, event_id):
    context = RequestContext(request)
    event = Exhibitions.objects.get(id=event_id)
    if event:
        context['event'] = event
        context['event_poster'] = '/static/images/0420-forum.jpg'
    else:
        context['event_name'] = u'None'
    return render_to_response('resources/event_landing.html', context)


def registerEvent(request):
    context = RequestContext(request)
    context.update(csrf(request))
    dictError = dict()
    if request.method == "POST":
        response = recaptcha_verify(request)
        if response["status"]:
            form = ForumRegistersForm(request.POST)
            if form.is_valid():
                form_data = form.cleaned_data
                register = ForumRegisters()
                register.user_name = form_data['user_name']
                register.user_mail = form_data['user_mail']
                register.telephone = form_data['telephone']
                register.telephone_sub = form_data['telephone_sub']
                register.cellphone = form_data['cellphone']
                register.company = form_data['company']
                register.address = form_data['address']
                register.identity= form_data['identity']
                register.industry= industries[int(form_data['industry'])][1]
                register.department = form_data['department']
                register.title = form_data['title']
                register.stages = stages[int(form_data['stages'])][1]
                register.agreed_contact =form_data['agreed_c']
                register.save()
                info(request, _("Successfully Registered."))
                responseHttp = HttpResponseRedirect("/events/regSuccess/")
                responseHttp.set_cookie("user_id", value=register.id, max_age=60)
                return responseHttp
            else:
                dictError['isExist'] = True
                dictError['type'] = 2
                dictError['errors'] = form.errors
                context['results'] = dictError
                context['form'] = ForumRegistersForm(request.POST)
        else:
            dictError['isExist'] = True
            dictError['type'] = 1
            dictError['errors'] = response['message']
            context['results'] = dictError
            context['form'] = ForumRegistersForm(request.POST)
            # print u"recaptcha error: %s" % s_info
    else:
        dictError['isExist'] = False
        context['results'] = dictError
        context['form'] = ForumRegistersForm(request.POST)

    return render_to_response('resources/register_form.html', context)


def registerSuccess(request):
    context = RequestContext(request)
    if request.COOKIES.has_key('user_id'):
        register_id = request.COOKIES['user_id']
        if register_id is None:
            main_title = "Your token has expired."
            msg = "Please back to our landing page."
            context['link_type'] = True
            isSuccess = False
        else:
            try:
                ForumRegisters.objects.get(id=register_id)
                register = ForumRegisters.objects.get(id=register_id)
                main_title = "Thanks for Your Registration!"
                msg = register
                isSuccess = True
            except ForumRegisters.DoesNotExist:
                main_title = "Registration has problems."
                msg = "We don't record your registration successfully. Please Try it Again."
                context['link_type'] = False
                isSuccess = False
        context['main'] = main_title
        context['msg'] = msg
        context['isSuccess'] = isSuccess
        return render_to_response('resources/register_success.html', context)
    else:
        context['main'] = "Permission Denied"
        context['msg'] = "You can't be here without our token."
        context['isSuccess'] = False
        return render_to_response('resources/register_success.html', context)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        remote_ip = x_forwarded_for.split(',')[0]
    else:
        remote_ip = request.META.get('REMOTE_ADDR')

    return remote_ip


def recaptcha_verify(request):
    if request.method == "POST":
        response = {}
        captcha_rs = request.POST['g-recaptcha-response']
        url = "https://www.google.com/recaptcha/api/siteverify"
        params = {
            'secret': '6LdiOBQUAAAAALkFcLL3aG7qaiiXbLPsQEB8xb39',
            'response': captcha_rs,
            'remoteip': get_client_ip(request)
        }
        verify_rs = requests.get(url, params=params, verify=True)
        verify_rs = verify_rs.json()
        response["status"] = verify_rs.get("success", False)
        response['message'] = verify_rs.get('error-codes', None)
        return response
        #return HttpResponse(response)