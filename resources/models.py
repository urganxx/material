# -*- coding: utf-8 -*-
from django.db import models
import datetime


class Customers(models.Model):
    company = models.CharField(max_length=255 , verbose_name='Company Name')
    #first_name = models.CharField(max_length=50,verbose_name='First Name')
    #last_name = models.CharField(max_length=50,verbose_name='Last Name')
    email = models.EmailField(verbose_name='Email Address',unique=True)
    bill_address = models.TextField(verbose_name='Billing Address', blank=True)
    #payment_method = models.IntegerField(verbose_name='Payment Method',max_length=2, default=0)
    #status = models.BooleanField(verbose_name='Payment Status', max_length=2, default=0)
    phoneNum = models.CharField(max_length=20, verbose_name='Phone Number')

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.company, self.email, self.bill_address, self.phoneNum)


class Supported(models.Model):
    customer = models.ForeignKey(Customers)
    isOpen = models.BooleanField(verbose_name=u'是否開放下載')
    platforms = models.CharField(verbose_name=u'OS平台', max_length= 20)
    launch_date = models.DateField(verbose_name=u'開放日期')
    closed_date = models.DateField(verbose_name=u'關閉日期')

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.id, self.customer, self.platforms, self.launch_date)


class ForumRegisters(models.Model):
    user_name = models.CharField(verbose_name=u'登記者名字', max_length= 40)
    user_mail = models.EmailField(verbose_name=u'登記者信箱', max_length= 40)
    telephone = models.CharField(verbose_name=u'登記者電話', max_length= 20)
    telephone_sub = models.CharField(verbose_name=u'分機編號', max_length= 6)
    cellphone = models.CharField(verbose_name=u'登記者手機', max_length= 10)
    company = models.CharField(verbose_name=u'公司名稱', max_length= 50)
    address = models.CharField(verbose_name=u'公司地址', max_length= 150)
    identity = models.CharField(verbose_name=u'公司統編', max_length= 10)
    industry = models.CharField(verbose_name=u'公司產業', max_length= 40)
    department = models.CharField(verbose_name=u'公司部門', max_length= 40)
    title = models.CharField(verbose_name=u'公司職稱', max_length= 40)
    stages= models.CharField(verbose_name=u'參加場次', max_length= 40)
    register_date = models.DateTimeField(verbose_name=u'公司職稱', auto_now_add=True)
    agreed_contact = models.BooleanField(verbose_name=u'同意聯繫')

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.id, self.user_name, self.user_mail, self.cellphone)


class Solution(models.Model):
    name = models.CharField(verbose_name=u'應用領域', max_length= 40)
    feature = models.ImageField(verbose_name=u'場景圖', max_length=250, upload_to='static/images/applications/', blank=True)
    overview = models.TextField(verbose_name=u'應用解釋', blank=True)
    redirect = models.URLField(verbose_name=u'轉址網址', blank=True)

    def __unicode__(self):
        return u'%s,%s,%s,%s' % (self.name, self.feature, self.overview, self.redirect)


class Technology(models.Model):
    title = models.CharField(verbose_name=u'標題', max_length= 40)
    subtitle = models.CharField(verbose_name=u'副標題', max_length= 60)
    introduce = models.TextField(verbose_name=u'說明', blank=True)
    icon_tag = models.CharField(verbose_name=u'iconTag', max_length=10)