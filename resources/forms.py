# -*- coding: utf-8 -*-
__author__ = 'urganxx'
from django.utils.safestring import mark_safe
from django.contrib.auth import authenticate
from django import forms
from django.forms.widgets import RadioSelect, TextInput, HiddenInput, CheckboxInput
from django.forms.extras.widgets import SelectDateWidget

industries = (
    (0, u'公司產業(Industry)'),(1, u'電腦與週邊'),(2, u'消費性電子'),(3, u'工業電腦/汽車電子'),(4, u'網路通訊'),
    (5, u'半導體'),(6, u'光電產品'),(7, u'零組件'),(8, u'電信服務'),(9, u'軟體/資訊服務'),
    (10, u'安全產業'),(11, u'量測儀器/生產設備'),(12, u'流通/運輸/倉儲'),(13, u'3C產品/零組件批發零售'),(14, u'電機/電器電纜'),
    (15, u'運輸工具及零組件製造'),(16, u'建築營造業'),(17, u'醫療'),(18, u'金融及保險業'),(19, u'軍方'),
    (20, u'教育/學術研究機構'),(21, u'運動休閒/娛樂產品製造業'),(22, u'專業、科學及技術服務業'),(23, u'大眾傳播'),(24, u'其它'),
)

stages = (
    #(0, u'全程參加'),
    (0, u'上午場(已取消)'),
    (1, u'下午場'),
)

class ForumRegistersForm(forms.Form):
    user_name = forms.CharField(label=u'登記者名字', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Name (required)'}), required=True)
    user_mail = forms.EmailField(label=u'登記者信箱', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Email (required)'}), required=True)
    telephone = forms.CharField(label=u'登記者電話', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Tel (alternative)'}), required=False)
    telephone_sub = forms.CharField(label=u'分機編號', widget=TextInput(attrs={'class':'form-control', 'placeholder': '#Sub'}), required=False)
    cellphone = forms.CharField(label=u'登記者手機', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Cell (alternative)'}), required=False)
    company = forms.CharField(label=u'公司名稱', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Company (required)'}), required=True)
    address = forms.CharField(label=u'公司地址', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Address (required)'}), required=True)
    identity= forms.CharField(label=u'公司統編', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'GUI number (required)'}), required=False)
    industry= forms.ChoiceField(label=u'公司產業', choices=industries, widget=forms.Select(attrs={'class':'form-control'}))
    agreed_c=forms.BooleanField(label=u'同意聯繫', widget=CheckboxInput(attrs={'class':''}), required=True)
    department = forms.CharField(label=u'公司部門', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Department (required)'}), required=True)
    title = forms.CharField(label=u'公司職稱', widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Title (required)'}), required=True)
    stages= forms.ChoiceField(label=u'參加場次', choices=stages, widget=forms.Select(attrs={'class':'form-control'}), )