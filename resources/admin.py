# -*- coding: utf-8 -*-
__author__ = 'urganxx'

from models import Customers, Supported, ForumRegisters
from django.contrib import admin

class CustomersAdmin(admin.ModelAdmin):
    list_display = ('company', 'email', 'bill_address', 'phoneNum')

class SupportedAdmin(admin.ModelAdmin):
    list_display = ('customer', 'isOpen', 'platforms', 'launch_date', 'closed_date')


class ForumRegistersAdmin(admin.ModelAdmin):
    list_display = ('user_name','user_mail','telephone','telephone_sub','cellphone','identity','company','address', 'industry', 'department', 'title', 'stages', 'register_date', 'agreed_contact')

admin.site.register(Customers, CustomersAdmin)
admin.site.register(Supported, SupportedAdmin)
admin.site.register(ForumRegisters, ForumRegistersAdmin)