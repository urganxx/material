
var margin = {top: 30, right: 40, bottom: 30, left: 50},
width = 380 - margin.left - margin.right,
height = 300 - margin.top - margin.bottom;

var parseDate = d3.time.format("%d-%b-%y").parse;

var x  = d3.scale.linear().range([0, width]);
var y0 = d3.scale.linear().range([height, 0]);
var y1 = d3.scale.linear().range([height, 0]);

var xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(4);

var yAxisLeft = d3.svg.axis().scale(y0).orient("left").ticks(5);

var yAxisRight = d3.svg.axis().scale(y1).orient("right").ticks(4);

var svg = d3.select("#diagram")
.append("svg").attr("width", width + margin.left + margin.right)
.attr("height", height + margin.top + margin.bottom)
.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Scale the range of the data
x.domain([0, 180]);
y0.domain([0, 40]);
y1.domain([0, 3]);

d3.csv("data2.csv", function(error, data) {
    data.forEach(function(d) {
        d.date = d.date;
        d.close = d.close;
        d.open = d.open;
    });
    console.log(data);
    // Scale the range of the data
    x.domain([0, 180]);
    y0.domain([0, 40]);
    y1.domain([0, 3]);

    //svg.append("path").attr("d", valueline(data));
    //svg.append("path").style("stroke", "red").attr("d", valueline2(data));
    svg.append("ellipse").attr("cx", x(60)).attr("cy", y0(10)).attr('rx', 80).attr('ry', 50).style("fill", "green")
    svg.append("text").attr("x", x(20)).attr("y", y0(10)).style({fill:'white','font-size': '16px'}).text("Phase BF Model");

    svg.append("ellipse").attr("cx", x(120)).attr("cy", y0(20)).attr('rx', 80).attr('ry', 50).style("fill", "blue");
    svg.append("text").attr("x", x(80)).attr("y", y0(20)).style({fill:'white','font-size': '16px'}).text("Adaptive BF Model");

    svg.append("ellipse").attr("cx", x(60)).attr("cy", y0(30)).attr('rx', 80).attr('ry', 50).style("fill", "red");
    svg.append("text").attr("x", x(20)).attr("y", y0(30)).style({fill:'white','font-size': '16px'}).text("High Gain Model");

    svg.append("g")            // Add the X Axis
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .style("fill", "white")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .style("fill", "steelblue")
        .call(yAxisLeft);

    svg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + width + " ,0)")
        .style("fill", "red")
        .call(yAxisRight);

});
