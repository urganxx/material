$(document).ready(function(){

	$.PanelSlider('.itemBar', 'slow');
	$.get_subCate('.mainCat');
	$.showDelete('.label-group', '.rmlab');

	$('.cloneRow').click(function(){
		var row = $(this).parents('.inBox-body');
		var ttt = row.find('.attr_row').clone();
		row.find('tbody').append('<tr class="row">'+ttt.html()+'</tr>');
		$.get_subCate('.mainCat');
	});


	$('.plus-cate').click(function(){
		var row = $(this).parents('.inBox-body');
		var main = row.find('select[name=mainCat]').val();
		var dd = $(this).parents('.test1').serialize();
		var sub = row.find('select[name=subCat]').val();
		var csrftoken = $.cookie('csrftoken');
		var cates = {'main': 1, 'sub': 2 };
		$.ajax({
			url: '/cooperators/saveEvtAttrs/',
			type: 'POST',
			dataType: 'json',
			data: dd,
			beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                    // Send the token to same-origin, relative URLs only.
                    // Send the token only if the method warrants CSRF protection
                    // Using the CSRFToken value acquired earlier
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
			success: function(result){
				if(result.sign){
					alert('success');
					location.reload();
				}else{
					alert(result.msg);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
            }
		});
	});

	$('.label-group > .rmlab').click(function(){
		alert('Create Delete Event!');
	});
});

$.showDelete = function(el,tel){
	var startDiv = $(el);
	startDiv.hover(function(){
		$(this).find(tel).show();
	},function(){
	    $(this).find(tel).hide();
	});
};

$.PanelSlider = function(el){
    var startPanel = $(el);
    var itemBar_heading = startPanel.find('.bar-panel-heading');
    itemBar_heading.click(function(){
        $(this).parent().find('.inBox-body').fadeToggle( 500,'swing' /*, function(){ alert('completed'); }*/ );
    });
};

$.get_subCate = function(el){
	$(el).change(function(){
	    var main_id = $(this).find(':selected').val();
	    var subSelect = $(this).parents('tr').find('.subCat');
	    subSelect.find('option').remove();
	    html ='';
	    $.getJSON('/cooperators/get_subcategory/'+main_id+'/', function(data){
	        for( var i=0; i < data.length; i++){
	            html += '<option value="'+data[i].id+'">'+ data[i].name +'</option>';
	        }
	        subSelect.append(html);
	    });
    });
}
