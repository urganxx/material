$(document).ready(function(){
	$('#mainCat').change(function(){
		var main_id = $('#mainCat :selected').val();
		$('#subCat option').remove();
		html ='';
		$.getJSON('/cooperators/get_subcategory/'+main_id+'/', function(data){
			for( var i=0; i < data.length; i++){
				html += '<option value="'+data[i].id+'">'+ data[i].name +'</option>';
			}
			$('#subCat').append(html);
		});
	});


	$('#tags-input').keypress(function(e){
		if(e.keyCode == 13){
			var tags = $(this).val();
			if(tags !== ''){
				var labels = '<span class="label label-inline">'+tags+'<i class="mdi-content-clear"></i></span>';
				$('#labels').append(labels);
				var labels = $('#labels .label-inline');
				var hiddens = [];
				for(var i=0; i<labels.length; i++){
					hiddens[i] = $(labels[i]).text();
				}
				$('#tags_hidden').val(JSON.stringify(hiddens));
				$(this).val('');

			}
			$('.mdi-content-clear').click(function(){ $(this).parent().remove(); });
			e.preventDefault();
		}
	});

	if ($('#labels .label-inline').length > 0){
		var labels = $('#labels .label-inline');
		var hiddens = [];
		for(var i=0; i<labels.length; i++){
			hiddens[i] = $(labels[i]).text();
		}
		$('#tags_hidden').val(JSON.stringify(hiddens));
		$('.mdi-content-clear').click(function(){ $(this).parent().remove(); });
	}
});
