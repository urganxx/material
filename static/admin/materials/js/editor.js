
function rm_cate_action(main, sub){
	main = main || 0;
	sub = sub || 0;
	var record = {"main":main, "sub": sub };
	var html = $('#checked').clone();
	$('body').append(html);
	html.addClass('in').show();
	html.find('#submitBtn').click(function(){
		submitClick(record);
		html.find('.dismissBtn').trigger('click');
	});
}

function submitClick(record, reqUrl){
	var csrftoken = $.cookie('csrftoken');
	$.ajax({
        url: reqUrl,
        type: 'POST',
        dataType: 'json',
        data: { "form_data": record },
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function(result){
            console.log(result);
            //location.reload(location.href);
        }
    });
}

function editClick(reqUrl, dom){
    var csrftoken = $.cookie('csrftoken');
    var sss;
    $.ajax({
        url: reqUrl,
        type: 'POST',
        dataType: 'json',
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function(result){
            $.each(result, function(i, field){
                dom.find('#id_'+ i).val(field);
            });

            //location.reload(location.href);
        }
    });
}