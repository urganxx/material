# -*- coding: utf-8 -*-
# Create your views here.
from django.template import RequestContext
from django.http import HttpResponse, Http404, HttpResponseNotFound
from django.shortcuts import render, redirect, render_to_response
from django.core.context_processors import csrf
from django.core.paginator import Paginator
from models import Spec, Stories, Categories, Scenario


def index(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['products'] = Spec.objects.all()

    return render_to_response('products/detail_pp01.html', context)


def service(request):
    context = RequestContext(request)
    context.update(csrf(request))
    context['products'] = Spec.objects.all()

    return render_to_response('solutions/index.html', context)


def pp02(request):
    context = RequestContext(request)
    context.update(csrf(request))
    return render_to_response('products/detail_pp02.html', context)


def mpcie(request):
    context = RequestContext(request)
    context.update(csrf(request))
    return render_to_response('products/pcie_intro.html', context)


def mlpb28dl(request):
    context = RequestContext(request)
    context.update(csrf(request))
    return render_to_response('products/ML60PRU3.html', context)


def m2card(request):
    context = RequestContext(request)
    context.update(csrf(request))
    return render_to_response('products/MLPBM2.html', context)


def mlpm01(request):
    context = RequestContext(request)
    context.update(csrf(request))
    return render_to_response('products/MLPM01.html', context)


def collection(SpecRaw):
    spec = dict()
    spec['field'] = SpecRaw.story.category.fields
    spec['name'] = SpecRaw.name
    spec['desc'] = SpecRaw.story.product_desc
    spec['download'] = SpecRaw.story.product_link
    spec['picture'] = SpecRaw.story.product_image
    comments = SpecRaw.comments
    comlist = comments.split("\n")
    half = len(comlist)/2
    pages = Paginator(comlist, half)
    page_left = pages.page(1)
    page_right = pages.page(2)
    spec['comment_left'] = page_left.object_list
    spec['comment_right'] = page_right.object_list
    return spec


def networkView(request):
    context = RequestContext(request)
    context.update(csrf(request))
    scenario = Scenario.objects.get(id=1)
    context['main'] = scenario
    category = Categories.objects.filter(scenario=scenario)
    context['fields'] = category
    cate_set = list()
    for row in category:
        # tmp = list()
        product = Stories.objects.get(category=row)
        specs = Spec.objects.filter(story=product)
        tmp = [collection(spec) for spec in specs]
        # tmp.append()
        cate_set.append(tmp)

    context['products'] = cate_set
    context['isPortfolio'] = True
    return render_to_response('products/scenario.html', context)


def vitrualizedView(request):
    context = RequestContext(request)
    context.update(csrf(request))
    scenario = Scenario.objects.get(id=2)
    context['main'] = scenario
    category = Categories.objects.filter(scenario=scenario)
    context['fields'] = category
    context['isPortfolio'] = False

    cate_set = list()
    for row in category:
        # tmp = list()
        product = Stories.objects.get(category=row)
        specs = Spec.objects.filter(story=product)
        tmp = [collection(spec) for spec in specs]
        cate_set.append(tmp)

    context['products'] = cate_set
    context['isPortfolio'] = True
    return render_to_response('products/scenario.html', context)


def specView(request, spec_id):
    context = RequestContext(request)
    context.update(csrf(request))
    try:
        rawData = Spec.objects.get(id=spec_id)
        context.update(setLayout(rawData))
        context['list'] = Spec.objects.all().exclude(id=spec_id)
        return render_to_response('products/portfolio-single-wide.html', context)
    except Spec.DoesNotExist:
        raise Http404()
        return HttpResponseNotFound(status=404)


def setLayout(objs):
    inputCont = {}
    inputCont['Head_Title'] = objs.name
    inputCont['Eng_Title'] = objs.eng_name
    inputCont['feature_pic'] = objs.flowchart
    inputCont['launch_date'] = objs.launch_date
    inputCont['platforms'] = objs.platforms.split(',')
    inputCont['supports'] = objs.support.split(',')
    inputCont['speeds'] = objs.speeds
    inputCont['Interface'] = objs.interface.split(',')
    inputCont['comments'] = objs.comments
    inputCont['distance'] = objs.distance
    return inputCont


def stories(request):
    context = RequestContext(request)
    rawData = Stories.objects.all().order_by('-id')
    context['portfolio'] = rawData

    storyList = Paginator(rawData, 6)
    page_num = 1
    if request.GET.get('page'):
        page_num = request.GET.get('page')

    page_list = storyList.page(page_num)
    context['current'] = page_num
    context['total_page'] = storyList.num_pages
    context['pagination'] = storyList.page_range
    if context['total_page'] > page_num:
        context['next'] = page_num + 1

    context['cates'] = Categories.objects.all()
    context['portfolio'] = page_list.object_list
    return render_to_response('products/story_list.html', context)
