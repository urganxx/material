# -*- coding: utf-8 -*-
__author__ = 'urganxx'

from models import Spec, Stories, Categories
from django.contrib import admin

class SpecAdmin(admin.ModelAdmin):
    fields = ('name', 'eng_name', 'interface','coverage','distance','speeds','flowchart', 'support','platforms','comments','prices')

class StoriesAdmin(admin.ModelAdmin):
    fields = ('product_name', 'product_image', 'product_link','product_desc','category')

class CategoriesAdmin(admin.ModelAdmin):
    fields = ('fields', 'subClass', 'hashtag')

admin.site.register(Spec, SpecAdmin)
admin.site.register(Stories, StoriesAdmin)
admin.site.register(Categories, CategoriesAdmin)