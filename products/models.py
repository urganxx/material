# -*- coding: utf-8 -*-
from django.db import models
import datetime


class Scenario(models.Model):
    mainTitle = models.TextField(verbose_name=u'主類別')
    description = models.TextField(verbose_name=u'情境敘述')
    featureImg = models.ImageField(max_length=250, verbose_name=u'情境圖', upload_to='static/content/scenarios/')

    def __unicode__(self):
        return u'%s, %s' % (self.mainTitle, self.description)


class Categories(models.Model):
    fields = models.CharField(max_length=50, verbose_name=u'領域')
    subClass = models.CharField(max_length=50, verbose_name=u'次分類')
    hashtag = models.CharField(max_length=50, verbose_name=u'標籤')
    scenario = models.ForeignKey(Scenario, verbose_name=u'產品應用領域')
    # products = models.CharField(max_length=50, verbose_name=u'產品')

    def __unicode__(self):
        return u'%s,%s,%s' % (self.fields, self.subClass, self.hashtag)


class Stories(models.Model):
    product_name = models.CharField(max_length=50, verbose_name=u'產品名稱')
    product_image = models.ImageField(max_length=250, verbose_name=u'產品圖', upload_to='static/images/scenarios/')
    product_link = models.URLField(verbose_name=u'產品外部連結')
    product_desc = models.TextField(verbose_name=u'產品敘述')
    category = models.ForeignKey(Categories, verbose_name=u'產品應用領域')

    def __unicode__(self):
        return u'%s, %s, %s, %s'%(self.product_name, self.product_image, self.product_link, self.product_desc)


class Spec(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'中文名字')
    eng_name = models.CharField(max_length=50, verbose_name=u'英文名字')
    interface = models.CharField(max_length=50, verbose_name=u'使用介面')
    coverage = models.IntegerField(max_length=4, verbose_name=u'覆蓋角度')
    distance = models.CharField(max_length=50, verbose_name=u'使用距離')
    speeds = models.CharField(max_length=50, verbose_name=u'傳輸速度')
    launch_date = models.DateField(auto_now=True, blank=False, verbose_name=u'上市時間')
    support = models.CharField(max_length=50, verbose_name=u'支援版本')
    platforms = models.CharField(max_length=50, verbose_name=u'支援平台')
    flowchart = models.ImageField(max_length=250, verbose_name=u'流程圖', upload_to='static/images/flowchart/')
    comments = models.TextField(verbose_name=u'備註')
    prices = models.IntegerField(verbose_name=u'公定價')
    story = models.ForeignKey(Stories, verbose_name=u'產品應用領域')

    def __unicode__(self):
        return u'%s,%s,%s,%s,%s,%s'%(self.name,self.eng_name,self.interface, self.platforms, self.support, self.launch_date)

