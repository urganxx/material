__author__ = 'urganxx'
from django.conf.urls import patterns, include, url

#from settings import PROJECT_DIRNAME

urlpatterns = patterns('products.views',
    url( r'mlpb01/', 'pp02'),
    url( r'mlpb28dl/', 'mlpb28dl'),
    url( r'mlpbm2/', 'm2card'),
    url( r'mlpm01/', 'mlpm01'),
    url( r'networks/', 'networkView'),
    url( r'vitrualization/', 'vitrualizedView'),
    url( r'product/(?P<spec_id>\d+)/$', 'specView'),
    url( r'^$', 'index' ),
)
