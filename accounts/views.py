# -*- coding: utf-8 -*-
# Create your views here.
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.core.context_processors import csrf
from admin import ProfileAdmin, CustomersAdmin
from models import Profile

def index(request):
    context = RequestContext(request)
    context.update(csrf(request))

    return render_to_response('portfolio-4cols.html',context)