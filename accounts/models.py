# -*- coding: utf-8 -*-
from django.db import models
import datetime
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.ForeignKey(User, unique=True, verbose_name=u'所屬帳號')
    name = models.CharField(max_length=50,verbose_name=u'中文名字')
    sexual = models.CharField(max_length=2,verbose_name=u'性別')
    birth_year = models.CharField(max_length=11,blank=True,verbose_name=u'出生年月')
    register_date = models.DateTimeField(auto_now_add=True,blank=False,verbose_name=u'建檔日期')
    last_login = models.DateTimeField(auto_now=True,blank=False,verbose_name=u'更新時間')
    certification = models.CharField(max_length=50,verbose_name=u'註冊認證')

    def __unicode__(self):
        return u'%s,%s,%s,%s,%s,%s'%(self.user,self.name,self.birth_year,self.register_date,self.last_login,self.certification)

