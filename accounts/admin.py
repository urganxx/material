# -*- coding: utf-8 -*-
__author__ = 'urganxx'

from django.db import models
from django.contrib import admin


class ProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'name', 'sexual', 'birth_year', 'certification')


class CustomersAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name', 'sexual', 'birth', 'company', 'email', 'bill_address', 'payment_method', 'status', 'phoneNum')


#admin.site.register(Profile, ProfileAdmin)
#admin.site.register(Customers, CustomersAdmin)
