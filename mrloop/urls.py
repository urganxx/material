from django.conf.urls import patterns, include, url
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from views import index, contacts, support, thankQ_page, login, logout
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Shuiyen.views.home', name='home'),
    # url(r'^Shuiyen/', include('Shuiyen.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^Admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^about/', include('about.urls')),
    # url(r'^news/view/(?P<news_id>\d+)$', 'about.views.detail_view' ),
    # url(r'^news/', 'about.views.index'),
    # url(r'^functions/(?P<spec_id>\d+)/$', 'products.views.specView'),
    # url(r'^stories/', 'products.views.stories'),
    # url(r'^solutions/mpcie', 'products.views.mpcie'),
    # url(r'^solutions/datasheet/', 'products.views.rfmodule'),
    # url(r'^solutions/', 'products.views.index'),
    url(r'^solutions/', include('products.urls')),
    url(r'^applications/', 'resources.views.applications'),
    url(r'^technology/', 'resources.views.technology'),
    # url(r'^resources/', include('resources.urls')),
    url(r'^events/(?P<event_id>\d+)/$', 'resources.views.event_page'),
    url(r'^events/register/', 'resources.views.registerEvent'),
    url(r'^events/regSuccess/$', 'resources.views.registerSuccess'),
    url(r'^contactus/', contacts),
    url(r'^support/', support),
    # url(r'^client_feedback/', 'views.client_feedback'),
    # url(r'^thank_you/', 'views.thankQ_page'),
    url(r'^login/', login),
    url(r'^logout/', logout),
    url(r'^$', index),
    url(r'^static/$', 'django.views.static.serve', {'document_root': settings.STATICFILES_DIRS}),
)
