# Create your views here.
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import login_required

from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from admins.forms import AuthenticationForm
from django.core.context_processors import csrf
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.contrib.messages import info, error
from datetime import datetime, timedelta, date
from about.models import Exhibitions, LatestLog
from products.models import Spec
from resources.models import Solution
import random


def index(request, template='home.html'):
    context = RequestContext(request)
    context['path'] = '/'
    context['latest'] = LatestLog.objects.all().order_by('-pubDate')[:3]
    context['events'] = Exhibitions.objects.filter(BeginDate__gte=datetime.today()).order_by('-id')[:3]
    # context['products'] = Spec.objects.all()[:4]
    sols = Solution.objects.all()
    context['products'] = random.sample(sols, 4)
    context['Event_Ad'] = False
    context['OpenFuncBlock'] = False
    #if len(context['events']) > 0:
    #    context['Event_Ad'] = context['events'][0]
    #return render_to_response(template, context)
    return redirect('http://www.millitronic.com.tw')


def contacts(request, template = 'contacts/contact.html'):
    context = RequestContext(request)
    #context.update(csrf(request))

    return render_to_response(template,context)


def support(request, template = 'contacts/contact.html'):
    context = RequestContext(request)
    #context.update(csrf(request))

    return render_to_response(template, context)

def client_feedback(request):
    context = RequestContext(request)
    context.update(csrf(request))

    return redirect("/thank_you/")

def thankQ_page(request):
    template = 'contacts/thank4feedback.html'
    return render_to_response(template)

def login(request):
    context = RequestContext(request)
    context.update(csrf(request))
    if request.user.is_active:
        return redirect("/")

    if request.method == "POST":
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            username = form_data['username']
            password = form_data['password']
            user = authenticate(username=username,password=password)
            if user is not None and user.is_active:
                auth_login(request, user)
                info(request, _("Successfully logged in"))
                return redirect("/Admin/")
            else:
                error(request,_("No Matched Account!"))
                return redirect("/")
        else:
            context['title'] = _("Log in")
            context['login_form'] = AuthenticationForm(request.POST)
    else:
        context['title'] = _("Log in")
        context['isLogin'] = False
        context['login_form'] = AuthenticationForm()

    return render_to_response('adminPanel/login.html', context)

def logout(request):
    auth_logout(request)
    info(request, _("Successfully logged out"))
    return redirect("/")
